class Persona:
    def __init__(self, n, a, f, d):
        self._nombrenombre = n
        self._apellido = a
        self._fecha_nacimiento = f
        self._dni = d

    def setNombre(self, n):
        self._nombre = n
    def getNombre(self):
        return self._nombre
    def setApellido(self, a):
        self._apellido = a
    def getApellido(self):
        return self._apelido
    def setFechaNacimiento(self, f):
        self._fecha_nacimiento = f
    def getFechaNacimiento(self):
        return self._fecha_nacimiento
    def setDni(self, d):
        self._dni = d
    def getDni(self):
        return self._dni

    def __str__(self):
        return f'Nombre:{self._nombre}, Apellido:{self._apellido}, Fecha de nacimiento:{self._fecha_nacimiento}, DNI:{self._dni}'

class Paciente(Persona):
    def __init__(self, n, a, f, d, hc):
        super().__init__(n, a, f, d)
        self.historial_clinico = hc
    def ver_historial_clinico(self):
        return self.historial_clinico

class Medico(Persona):
    def __init__(self, n, a, f, d, e, c):
        super().__init__(n, a, f, d)
        self.especialidad = e
        self.citas = c
    def consultar_agenda(self):
        return f'Citas: {self.citas}'