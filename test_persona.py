import unittest

from persona import Paciente
from persona import Medico

persona1 = Paciente('Sara', 'Iturralde Pepa', '05/12/1984', '73648534P', 'gripe')
persona2 = Medico('Sara', 'Iturralde Pepa', '05/12/1984', '73648534P', 'pediatría', '1 marzo')
    
class TestPaciente(unittest.TestCase):
    def test_ver_historial_clinico(self):
        self.assertEqual(persona1.historial_clinico, "gripe")
    def test_ver_historial_clinico_mal(self):
        self.assertNotEqual(persona1.historial_clinico, "fractura")
class TestMedico(unittest.TestCase):
    def test_consultar_agenda(self):
        self.assertEqual(persona2.consultar_agenda(), "Citas: 1 marzo")
    def test_consultar_agenda_mal(self):
        self.assertNotEqual(persona2.consultar_agenda(), "Citas: 3 febrero")


unittest.main()